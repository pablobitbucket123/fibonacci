describe("Fibonacci", function (){
    var Fibonacci = require('../lib/Fibonacci');
    var fibonacci;

    beforeEach(function(){
       fibonacci = new Fibonacci();
    });

    it('returns 0 when input is 1, which is the first index', function(){
        expect(fibonacci.sumPreviousTwoNumbers(1)).toEqual(0);
    });

    it('returns 1 when input is index =2', function(){
        expect(fibonacci.sumPreviousTwoNumbers(2)).toEqual(1);
    });

    it('returns 1 when input is index =3 ', function(){
        expect(fibonacci.sumPreviousTwoNumbers(3)).toEqual(1);
    });

    it('returns 2 when input is index =4 ', function(){
        expect(fibonacci.sumPreviousTwoNumbers(4)).toEqual(2);
    });

    it('returns 3 when input is index =5 ', function(){
        expect(fibonacci.sumPreviousTwoNumbers(5)).toEqual(3);
    });

    it('returns 5 when input is index =6 ', function(){
        expect(fibonacci.sumPreviousTwoNumbers(6)).toEqual(5);
    });









})