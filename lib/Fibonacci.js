function Fibonacci (){

}

Fibonacci.prototype.sumPreviousTwoNumbers = function(index){
    //
    // var initialArray = [0, 1];
    //
    // for (var i = 2; i < index; i++ ){
    //     var sum = initialArray[i-2] + initialArray[i-1]
    //     initialArray.push(sum);
    // }
    //
    // return index === 1 ? 0
    //         : index === 2 ? 1
    //         : initialArray[initialArray.length -1];

    if(index == 1 )
        return 0;

    if(index == 2)
        return 1;

    return this.sumPreviousTwoNumbers(index - 1) + this.sumPreviousTwoNumbers(index - 2);

}

module.exports = Fibonacci;

